var mocks = {
  contact: function() {
    return {
      name: 'Amar',
      email: 'amardeep@live.com',
      phone: '408-555-1234',
      address_line_1: '555 Halford Ave.',
      address_line_2: 'Apartment #43C',
      city: 'Santa Clara',
      state: 'CA',
      zip: '95051',
      avatar_path: './assets/images/amar.gif'
    };
  }
};

describe('ContactWidget', function() {
  describe('#createContactListItemElement()', function() {
    var contact,
        contactListItemElement;

    beforeEach(function() {
      contact = mocks.contact();
    });

    it('should return an LI element ', function() {
      contactListItemElement = ContactWidget
        .createContactListItemElement(
          contact,
          ContactWidget.ContactEnum.FACET.EMAIL
        );

      expect(contactListItemElement.tagName)
        .toEqual('LI');
    });

    it('should return an element with a "contact" class', function() {
      contactListItemElement = ContactWidget
        .createContactListItemElement(
          contact,
          ContactWidget.ContactEnum.FACET.EMAIL
        );

      expect(contactListItemElement.className)
        .toEqual('contact');
    });


    it('should throw an error if the contactFacet is not email or phone', function() {
      expect(function() {
        ContactWidget
        .createContactListItemElement(
          contact,
          {
            name: 'Address Line 1',
            value: 'address_line_1'
          }
        );
      }).toThrowError('ContactWidget does not support Address Line 1 as a primary facet.');
    });

    it('should create left column in the returned element', function() {
      contactListItemElement = ContactWidget
        .createContactListItemElement(
          contact,
          ContactWidget.ContactEnum.FACET.EMAIL
        );

      expect(contactListItemElement.children[0].className)
        .toContain('left column')
    });

    it('should create right column in the returned element', function() {
      contactListItemElement = ContactWidget
        .createContactListItemElement(
          contact,
          ContactWidget.ContactEnum.FACET.EMAIL
        );

      expect(contactListItemElement.children[1].className)
        .toContain('right column')
    });

    it('should have a label containing the name in the left column of the returned element', function() {
      contactListItemElement = ContactWidget
        .createContactListItemElement(
          contact,
          ContactWidget.ContactEnum.FACET.EMAIL
        );

      expect(contactListItemElement.childNodes[0].innerHTML)
        .toEqual('<label class="facet">Amar</label>')
    });

    it('should have full address in the detail section of the right column', function() {
      expect(contactListItemElement.childNodes[1].childNodes[0].childNodes[2].innerHTML)
        .toContain('<p class="facet">555 Halford Ave. Apartment #43C</p>');
    });

    it('should have city state and zip in the detail section of the right column', function() {
      expect(contactListItemElement.childNodes[1].childNodes[0].childNodes[2].innerHTML)
        .toContain('<p class="facet">Santa Clara, CA 95051</p>');
    });

    it('should have the avatar in after the primary facet in the right column', function() {
      expect(contactListItemElement.childNodes[1].childNodes[0].childNodes[1].outerHTML)
        .toEqual('<img src="./assets/images/amar.gif" class="avatar">');
    });

    describe('when the selected facet is email', function() {
      beforeEach(function() {
        contactListItemElement = ContactWidget
          .createContactListItemElement(
            contact,
            ContactWidget.ContactEnum.FACET.EMAIL
          );
      });

      it('should have a mailto link containing the contacts email in the right column', function() {
        expect(contactListItemElement.childNodes[1].childNodes[0].childNodes[0].outerHTML)
          .toEqual('<a href="mailto:amardeep@live.com" class="facet">amardeep@live.com</a>');
      });

      it('should have phone number in the detail section of the right column', function() {
        expect(contactListItemElement.childNodes[1].childNodes[0].childNodes[2].innerHTML)
          .toContain('<span class="facet">408-555-1234</span>');
      });

      it('should not have the contacts email in the detail section of the right column', function() {
        expect(contactListItemElement.childNodes[1].childNodes[0].childNodes[2].innerHTML)
          .not.toContain('<a href="mailto:amardeep@live.com" class="facet">amardeep@live.com</a>');
      });
    });

    describe('when the selected facet is phone', function() {
      beforeEach(function() {
        contactListItemElement = ContactWidget
          .createContactListItemElement(
            contact,
            ContactWidget.ContactEnum.FACET.PHONE
          );
      });

      it('should have a span containing the contacts phone in the right column', function() {
        expect(contactListItemElement.childNodes[1].childNodes[0].childNodes[0].outerHTML)
          .toEqual('<span class="facet">408-555-1234</span>')
      });

      it('should have email in the detail section of the right column', function() {
        expect(contactListItemElement.childNodes[1].childNodes[0].childNodes[2].innerHTML)
          .toContain('<a href="mailto:amardeep@live.com" class="facet">amardeep@live.com</a>');
      });

      it('should not have the contacts email in the detail section of the right column', function() {
        expect(contactListItemElement.children[1].childNodes[0].childNodes[2].innerHTML)
          .not.toContain('<span class="facet">408-555-1234</span>');
      });
    });
  });

  describe('#createHeadingElement()', function() {
    var headingElement;

    beforeEach(function() {
      headingElement = ContactWidget.createHeadingElement('Contacts'); 
    });

    it('should return an H1 tag', function() {
      expect(headingElement.tagName)
        .toEqual('H1');
    });

    it('should return element containing given header text', function() {
      expect(headingElement.innerText)
        .toEqual('Contacts');
    });
  });

  describe('#createSelectElement()', function() {
    var onChangeSelect,
        selectElement,
        selectOptions;

    beforeEach(function() {
      onChangeSelect = jasmine.createSpy('onChangeSelect');

      selectOptions = [
        { name: 'Email', value: 'email' },
        { name: 'Phone Number', value: 'phone' }
      ];

      selectElement = ContactWidget
        .createSelectElement(selectOptions, selectOptions[1], onChangeSelect);
    });

    it('should return a SELECT tag', function() {
      expect(selectElement.tagName)
        .toEqual('SELECT');
    });

    it('should contain OPTION elements', function() {
      expect(selectElement.childNodes[0].tagName)
        .toEqual('OPTION');
    });

    it('should contain first option name in the first OPTION element', function() {
      expect(selectElement.childNodes[0].innerText)
        .toEqual(selectOptions[0].name)
    });

    it('should use first option value as the value attribute on the first OPTION element', function() {
      expect(selectElement.childNodes[0].value)
        .toEqual(selectOptions[0].value)
    });

    it('should contain second option name in the second OPTION element', function() {
      expect(selectElement.childNodes[1].innerText)
        .toEqual(selectOptions[1].name)
    });

    it('should use second option value as the value attribute on the second OPTION element', function() {
      expect(selectElement.childNodes[1].value)
        .toEqual(selectOptions[1].value)
    });

    it('should mark the option given as the second parameter as selected', function() {
      expect(selectElement.childNodes[1].getAttribute('selected'))
        .toEqual('selected');
    });

    it('should keep the option that was not given as the second parameter unselected', function() {
      expect(selectElement.childNodes[0].getAttribute('selected'))
        .toEqual(null);
    });

    it('should call onChangeSelect callback with selected option when the value changes', function() {
      selectElement.onchange({target: {value: 'phone'}});

      expect(onChangeSelect)
        .toHaveBeenCalledWith({
          name: 'Phone number',
          value: 'phone'
        });
    });
  });

  describe('when instantiated', function() {
    var contacts,
        containerElement,
        contactWidget;

    beforeEach(function() {
      contacts = [mocks.contact()];
      containerElement = document.createElement('div');

      contactWidget = new ContactWidget({
        contacts: contacts,
        container: containerElement,
      });
    });

    it('should have email number as first breakdown option', function() {
      expect(contactWidget.contactOverviewFacets[0])
        .toEqual({
          name: 'Email address',
          value: 'email'
        });
    });

    it('should have phone number as second breakdown option', function() {
      expect(contactWidget.contactOverviewFacets[1])
        .toEqual({
          name: 'Phone number',
          value: 'phone'
        });
    });

    it('should contain a footer as a child in the layout', function() {
      expect(contactWidget.layout.children.footer.element.tagName)
        .toEqual('FOOTER');
    });

    it('should contain a header as a child in the layout', function() {
      expect(contactWidget.layout.children.header.element.tagName)
        .toEqual('HEADER');
    });

    it('should contain an ordered list as a child in the layout', function() {
      expect(contactWidget.layout.children.list.element.tagName)
        .toEqual('OL');
    });

    it('should append the contact widget element to the given container', function() {
      expect(containerElement.childNodes[0])
        .toEqual(contactWidget.layout.element)
    });

    describe('#render()', function() {
      var contactListItemElement,
          headingElement,
          selectElement;

      beforeEach(function() {
        spyOn(ContactWidget, 'createContactListItemElement').and.callFake(function() {
          contactListItemElement = document.createElement('ol'); 

          return contactListItemElement;
        });

        spyOn(ContactWidget, 'createHeadingElement').and.callFake(function() {
          headingElement = document.createElement('div'); 

          return headingElement;
        });

        spyOn(ContactWidget, 'createSelectElement').and.callFake(function() {
          selectElement = document.createElement('select'); 

          return selectElement;
        });
      });

      it('should clear header in the layout before rendering', function() {
        contactWidget.layout.children.header.element.innerHTML = '<h1>An hold header</h1>';

        contactWidget.render();

        expect(contactWidget.layout.children.header.element.innerHTML)
          .not.toContain('<h1>An hold header</h1>');
      });

      it('should clear footer in the layout before rendering', function() {
        contactWidget.layout.children.footer.element.innerHTML = '<select><option>An old option</open></select>';

        contactWidget.render();

        expect(contactWidget.layout.children.footer.element.innerHTML)
          .not.toContain('<select><option>An old option</open></select>');
      });

      it('should clear footer in the layout before rendering', function() {
        contactWidget.layout.children.list.element.innerHTML = '<ol><li>An old contact</li></ol>';

        contactWidget.render();

        expect(contactWidget.layout.children.list.element.innerHTML)
          .not.toContain('<ol><li>An old contact</li></ol>');
      });

      it('should include a heading element in the header section', function() {
        contactWidget.render(contactWidget.contactOverviewFacets[0]);

        expect(contactWidget.layout.children.header.element.childNodes[0])
          .toBe(headingElement);
      });

      it('should create heading element with "Contacts"', function() {
        contactWidget.render(contactWidget.contactOverviewFacets[0]);

        expect(ContactWidget.createHeadingElement)
          .toHaveBeenCalledWith('Contacts');
      });

      it('should create heading element with "Contacts"', function() {
        contactWidget.render(contactWidget.contactOverviewFacets[0]);

        expect(ContactWidget.createHeadingElement)
          .toHaveBeenCalledWith('Contacts');
      });

      it('should create contact list element with contact', function() {
        contactWidget.render(contactWidget.contactOverviewFacets[0]);

        expect(ContactWidget.createContactListItemElement)
          .toHaveBeenCalledWith(contacts[0], ContactWidget.ContactEnum.FACET.EMAIL);
      });

      it('should include contact list elements in contact list', function() {
        contactWidget.render(contactWidget.contactOverviewFacets[0]);

        expect(contactWidget.layout.children.list.element.childNodes[0])
          .toBe(contactListItemElement);
      });

      it('should include a select element in the footer section', function() {
        contactWidget.render(contactWidget.contactOverviewFacets[0]);

        expect(contactWidget.layout.children.footer.element.childNodes[0])
          .toBe(selectElement);
      });

      it('should create select element with the rendered breakdown selected', function() {
        contactWidget.render(contactWidget.contactOverviewFacets[1])

        expect(ContactWidget.createSelectElement)
          .toHaveBeenCalledWith(contactWidget.contactOverviewFacets, {
            name: 'Phone number',
            value: 'phone'
          }, jasmine.any(Function));
      });
    });
  });
});
