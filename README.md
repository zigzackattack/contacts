Contact Widget
==============

## Use
To use ContactWidget in your app:

1. Include src/js/contact-widget.js in your project
1. Create an container element in the DOM that you can attach to.
1. Instantiate a new ContactWidget:

```javascript
var contactWidget = new ContactWidget({
  container: document.querySelector('#my-container'),
  contacts: myArrayOfContacts
});

// Render the email facet breakdown view
contactWidget.render(ContactWidget.ContactEnum.FACET.EMAIL);

// or Render the phone facet breakdown view
contactWidget.render(ContactWidget.ContactEnum.FACET.PHONE);

/**
 * @note: You can call `render()` programatically anywhere and any time in your code
 */
```

You can see an example in the example directory.

## Testing

To run the test suite:

1. Run `npm install` in the root projects directory
1. Run `npm test` in the root project directory
