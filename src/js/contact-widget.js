(function(exports) {
  'use strict';

  /**
   * @private
   * @name clearLayout
   * @description Clears all rendered sections from a layout
   * @param {Object} layout A layout to clear
   */
  function clearLayout(layout) {
    layout.children.footer.element.innerHTML = '';
    layout.children.header.element.innerHTML = '';
    layout.children.list.element.innerHTML = '';
  }

  /**
   * @static
   * @private
   * @name createContactLabel
   * @description Creates a contact name label
   * @param {String} contactName The name of the contact
   * @return {HTMLElement} The contact name label
   */
  function createContactLabelElement(contactName) {
    var contactLabelElement = document.createElement('label'),
        contactLabelTextNode = document.createTextNode(contactName);

    contactLabelElement.appendChild(contactLabelTextNode);
    contactLabelElement.className = 'facet';

    return contactLabelElement;
  }

  /**
   * @static
   * @private
   * @name createEmailLinkElement
   * @description Creates an email link
   * @param {String} email An email address
   * @return {HTMLElement} The email link
   */
  function createEmailLinkElement(email) {
    var emailLinkElement = document.createElement('a'),
        emailHref = [
          'mailto:',
          email
        ].join('');

    emailLinkElement.setAttribute('href', emailHref);

    return emailLinkElement;
  }

  /**
   * @static
   * @private
   * @name createContactFacetElement
   * @description Creates an element containing the given facet on the contact
   * @param {Object} contact The client to render an overview for
   * @param {Object} primaryFacet A facet enum entry
   * @return {HTMLElement} The contact facet element
   */
  function createContactFacetElement(contact, primaryFacet) {
    var primaryFacetText = contact[primaryFacet.value],
        primaryFacetTextNode = document.createTextNode(primaryFacetText),
        ContactFacetEnum = ContactWidget.ContactEnum.FACET,
        isFacetEmail = primaryFacet.value === ContactFacetEnum.EMAIL.value,
        isFacetPhone = primaryFacet.value === ContactFacetEnum.PHONE.value,
        primaryFacetElement;

    if (isFacetEmail) {
      primaryFacetElement = createEmailLinkElement(contact.email);
    } else if (isFacetPhone) {
      primaryFacetElement = document.createElement('span');
    } else {
      throw new Error([
        'ContactWidget does not support',
        primaryFacet.name,
        'as a primary facet.'
      ].join(' '));
    }

    primaryFacetElement.appendChild(primaryFacetTextNode);

    return primaryFacetElement;
  }

  /**
   * @static
   * @private
   * @name createSecondaryFacetGroupElement
   * @description Creates a secondary facets group element for the contact
   * @param {Object} contact The contact to display secondary facets for
   * @param {Object} primaryFacet The primary facet to omit from secondary facets
   * @return {HTMLElement} The secondary facets group element
   */
  function createSecondaryFacetGroupElement(contact, primaryFacet) {
    var addressElement = document.createElement('p'),
        ContactFacetEnum = ContactWidget.ContactEnum.FACET,
        cityStateZipElement = document.createElement('p'),
        isFacetEmail = primaryFacet.value === ContactFacetEnum.EMAIL.value,
        isFacetPhone = primaryFacet.value === ContactFacetEnum.PHONE.value,
        secondaryFacetGroupElement = document.createElement('div'),
        dynamicSecondaryFacetElement;

    if (isFacetEmail) {
      dynamicSecondaryFacetElement = document.createElement('span');
      dynamicSecondaryFacetElement.appendChild(document.createTextNode(contact.phone));
    } else if (isFacetPhone) {
      dynamicSecondaryFacetElement = createEmailLinkElement(contact.email);
      dynamicSecondaryFacetElement.appendChild(document.createTextNode(contact.email));
    }

    addressElement.appendChild(document.createTextNode([
      contact.address_line_1,
      contact.address_line_2
    ].join(' ')));

    cityStateZipElement.appendChild(document.createTextNode([
      contact.city + ',',
      contact.state,
      contact.zip
    ].join(' ')));

    dynamicSecondaryFacetElement
      .setAttribute('class', ContactWidget.StyleEnum.CONTACT.FACET.value);

    addressElement
      .setAttribute('class', ContactWidget.StyleEnum.CONTACT.FACET.value);

    cityStateZipElement
      .setAttribute('class', ContactWidget.StyleEnum.CONTACT.FACET.value);

    secondaryFacetGroupElement
      .setAttribute('class', ContactWidget.StyleEnum.CONTACT.DETAILS_SECONDARY.value);

    secondaryFacetGroupElement.appendChild(dynamicSecondaryFacetElement);
    secondaryFacetGroupElement.appendChild(addressElement);
    secondaryFacetGroupElement.appendChild(cityStateZipElement);

    return secondaryFacetGroupElement;
  }

  /**
   * @static
   * @private
   * @name createContactDetailElement
   * @description Creates the contact details element that displays when a user
                  hovers over a contact
   * @param {Object} contact The contact to show details for
   * @param {Object} primaryFacet The facet to omit from the contact details
   * @return {HTMLElement} The contact details element
   */
  function createContactDetailElement(contact, primaryFacet) {
    var contactDetailElement = document.createElement('div'),
        avatarElement = document.createElement('img'),
        primaryFacetElement,
        secondaryFacetGroupElement;

    primaryFacetElement = createContactFacetElement(
      contact,
      primaryFacet
    );

    secondaryFacetGroupElement = createSecondaryFacetGroupElement(
      contact,
      primaryFacet
    );

    avatarElement
      .setAttribute('src', contact.avatar_path);

    avatarElement
      .setAttribute('class', ContactWidget.StyleEnum.CONTACT.AVATAR.value);

    contactDetailElement
      .setAttribute('class', ContactWidget.StyleEnum.CONTACT.DETAILS.value);

    primaryFacetElement
      .setAttribute('class', ContactWidget.StyleEnum.CONTACT.FACET.value);

    contactDetailElement.appendChild(primaryFacetElement);
    contactDetailElement.appendChild(avatarElement);
    contactDetailElement.appendChild(secondaryFacetGroupElement);

    return contactDetailElement;
  }

  /**
   * @static
   * @public
   * @name createContactListItemElement
   * @description Creates a list item element for the contact and facet
   * @param {Object} contact The contact to display
   * @param {Object} primaryFacet The facet enum entry for the primary facet to
   *                              render in the list item
   * @return {HTMLElement} The list item element for the contact
   */
  function createContactListItemElement(contact, primaryFacet) {
    var contactListItemElement = document.createElement('li'),
        leftColumnElement = document.createElement('div'),
        rightColumnElement = document.createElement('div');

    leftColumnElement
      .setAttribute('class', [
        ContactWidget.StyleEnum.GRID.LEFT.value,
        ContactWidget.StyleEnum.GRID.COLUMN.value
      ].join(' '));

    rightColumnElement
      .setAttribute('class', [
        ContactWidget.StyleEnum.GRID.RIGHT.value,
        ContactWidget.StyleEnum.GRID.COLUMN.value
      ].join(' '));

    contactListItemElement
      .setAttribute('class', ContactWidget.StyleEnum.CONTACT.LIST_ITEM.value);

    contactListItemElement
      .appendChild(leftColumnElement);

    contactListItemElement
      .appendChild(rightColumnElement);

    leftColumnElement
      .appendChild(createContactLabelElement(contact.name));

    rightColumnElement
      .appendChild(createContactDetailElement(
        contact,
        primaryFacet
      ));

    return contactListItemElement;
  }

  /**
   * @static
   * @public
   * @name createHeadingElement
   * @param {String} headingText The text to display in the heading
   * @description Gets a heading with the given headingText
   * @return {HTMLElement} The heading with the given headingText 
   */
  function createHeadingElement(headingText) {
    var headingTextNode = document.createTextNode(headingText),
        headingElement = document.createElement('h1');

    headingElement.appendChild(headingTextNode);

    return headingElement;
  }

  /**
   * @static
   * @public
   * @name createSelectElement
   * @description Creates a select element with given options and marks 
   *              selectedOption as the default selection
   * @param {Array} options Options to list in the select element
   * @param {Object} selectedOption The option to select by default
   * @param {Function} onChangeSelect The callback for when the value of the
   *                                  select element changes. The callback is
   *                                  called with the selected option.
   * @return {HTMLElement} The select element
   */
  function createSelectElement(options, selectedOption, onChangeSelect) {
    var selectElement = document.createElement('select');
    
    selectElement.onchange = function(event) {
      var facetEnumKey = event.target.value.toUpperCase(),
          option = ContactWidget.ContactEnum.FACET[facetEnumKey];

      onChangeSelect(option);
    };

    options.forEach(function(option) {
      var optionElement = document.createElement('option'),
          optionTextNode = document.createTextNode(option.name);

      optionElement.appendChild(optionTextNode);
      optionElement.setAttribute('value', option.value);

      if (option === selectedOption) {
        optionElement.setAttribute('selected', 'selected');
      }

      selectElement.appendChild(optionElement);

    });

    return selectElement;
  }

  /**
   * @constructor ContactWidget
   * @description The ContactWidget constructor
   * @param {Object} configuration Configuration for the contact widget
   * @param {Array} configuration.contacts The contacts to display
   * @param {Array} configuration.container The element to render the widget in
   */
  function ContactWidget(configuration) {
    var layoutElement = document.createElement('section'),
        footerElement = document.createElement('footer'),
        headerElement = document.createElement('header'),
        listElement = document.createElement('ol');

    this.contactOverviewFacets = [
      ContactWidget.ContactEnum.FACET.EMAIL,
      ContactWidget.ContactEnum.FACET.PHONE
    ];

    this.contacts = configuration.contacts;

    this.layout = {
      element: layoutElement,

      children: {
        footer: {element: footerElement},
        header: {element: headerElement},
        list: {element: listElement}
      }
    };

    this.layout.element
      .setAttribute('class', ContactWidget.StyleEnum.LAYOUT.value);

    this.layout.children.list.element
      .setAttribute('class', ContactWidget.StyleEnum.CONTACT.LIST.value);

    this.layout.element
      .appendChild(this.layout.children.header.element);

    this.layout.element
      .appendChild(this.layout.children.list.element);

    this.layout.element
      .appendChild(this.layout.children.footer.element);

    configuration.container
      .appendChild(this.layout.element);
  }

  /**
   * @public
   * @name render
   * @description Renders the contact widget in its current state
   */
  ContactWidget.prototype.render = function(selectedContactFacet) {
    var _this = this,
        contactListItemElements = this.contacts.map(function(contact) {
          return ContactWidget
            .createContactListItemElement(contact, selectedContactFacet);
        }),
        headingElement = ContactWidget.createHeadingElement('Contacts'),
        selectElement = ContactWidget.createSelectElement(
          this.contactOverviewFacets,
          selectedContactFacet,
          function onChangeSelect(facet) {
            _this.render(facet);
          }
        );

    clearLayout(this.layout);

    this.layout.children.footer.element
      .appendChild(selectElement);

    this.layout.children.header.element
      .appendChild(headingElement);

    contactListItemElements.forEach(function(contactListItemElement) {
      _this.layout.children.list.element
        .appendChild(contactListItemElement);
    });
  };


  ContactWidget.StyleEnum = {
    LAYOUT: {
      name: 'Layout Namespace',
      value: 'contactWidget'
    },
    CONTACT: {
      AVATAR: {
        name: 'Contact Avatar',
        value: 'avatar'
      },
      FACET: {
        name: 'Contact Facet',
        value: 'facet'
      },
      DETAILS: {
        name: 'Contact Details',
        value: 'details'
      },
      DETAILS_SECONDARY: {
        name: 'Contact Secondary Details',
        value: 'secondary'
      },
      LIST: {
        name: 'Contact List',
        value: 'contactList'
      },
      LIST_ITEM: {
        name: 'Contact List Item',
        value: 'contact'
      }
    },
    GRID: {
      LEFT: {
        name: 'Grid Left Side',
        value: 'left'
      },
      RIGHT: {
        name: 'Grid Right Side',
        value: 'right'
      },
      COLUMN: {
        name: 'Grid Column',
        value: 'column'
      }
    }
  };

  ContactWidget.ContactEnum = {
    FACET: {
      EMAIL: {
        name: 'Email address',
        value: 'email'
      },
      PHONE: {
        name: 'Phone number',
        value: 'phone'
      }
    }
  };

  ContactWidget.createContactListItemElement = createContactListItemElement;
  ContactWidget.createHeadingElement = createHeadingElement;
  ContactWidget.createSelectElement = createSelectElement;

  exports.ContactWidget = ContactWidget;
})(window);
